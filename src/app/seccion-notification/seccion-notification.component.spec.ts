import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeccionNotificationComponent } from './seccion-notification.component';

describe('SeccionNotificationComponent', () => {
  let component: SeccionNotificationComponent;
  let fixture: ComponentFixture<SeccionNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeccionNotificationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SeccionNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
