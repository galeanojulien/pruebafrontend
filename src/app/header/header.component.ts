import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  logo = 'myLorem';
  elementos = [
   {welcome: 'Welcome' }, 
   {Dropdown:'DropDown'},
   {Leftsidebar:'LeftSidebar'},
   {Rightsidebar:'RightSidebar'},
   {Nosidebar: 'NoSidebar'}
 ]

}
