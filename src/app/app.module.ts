import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SeccionNotificationComponent } from './seccion-notification/seccion-notification.component';
import { SeccioncardsComponent } from './seccioncards/seccioncards.component';
import { SeccioninteresComponent } from './seccioninteres/seccioninteres.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SeccionNotificationComponent,
    SeccioncardsComponent,
    SeccioninteresComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
