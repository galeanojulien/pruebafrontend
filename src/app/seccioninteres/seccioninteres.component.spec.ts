import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeccioninteresComponent } from './seccioninteres.component';

describe('SeccioninteresComponent', () => {
  let component: SeccioninteresComponent;
  let fixture: ComponentFixture<SeccioninteresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeccioninteresComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SeccioninteresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
