import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeccioncardsComponent } from './seccioncards.component';

describe('SeccioncardsComponent', () => {
  let component: SeccioncardsComponent;
  let fixture: ComponentFixture<SeccioncardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeccioncardsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SeccioncardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
